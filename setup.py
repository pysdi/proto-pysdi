from setuptools import setup

setup(name='pysdi',
      version='1.3',
      description=('Package to generate maps and time series of univariate and'
                   'multivariate standardaized drought indexes'),
      url='https://bitbucket.org/rrealr/pysdi',
      author='Roberto A. Real-Rangel (Institute of Engineering UNAM, Mexico)',
      author_email='rrealr@iingen.unam.mx',
      license='MIT',
      packages=['pysdi'],
      install_requires=['markdown', ],
      zip_safe=False)
