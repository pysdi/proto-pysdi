# README #

### What is this repository for? ###
The code PYSDI.py and other ancillary codes compute the Standardized Drought Indexes for a given variable (e.g., precipitation, soil moisture, etc.) following the methodology proposed by McKee, Doesken & Kleist (1993) on a basis of a raster map. It is based on the code SDAT.mat published by Farahmand & AghaKouchak (2015) (available [HERE](http://amir.eng.uci.edu/sdat.php)).

PYSDI was developed as part of a Master in Engineering (Hydraulics) final project performed in the Institute of Engineering of the National Autonomous University of Mexico (Instituto de Ingeniería de la Universidad Nacional Autónoma de México, IIUNAM) and was funded by the National Commission of Water (Comisión Nacional del Agua, CONAGUA) of Mexico under the Specific Collaboration Agreement SGT-GASIR-DF-16-09-RF-CC for the Service of Droughts in Mexico mapping service (Servicio de elaboración de mapas de sequía en México).

### Who do I talk to? ###
For any issue, commentary or suggestion related to this code, please contact with Roberto A. Real-Rangel (developer) to the e-mail rrealr@iingen.unam.mx.

### References ###
Farahmand, A., AghaKouchak, A., 2015. A generalized framework for deriving nonparametric standardized drought indicators. Advances in Water Resources 76, 140–145. doi:10.1016/j.advwatres.2014.11.012

McKee, T. B., Doesken, N. J., Kleist, J., 1993. Drought monitoring with multiple timescales, in: Eighth Conf. on Applied Climatology. American Meteorological Society, Anaheim, CA, pp. 179–184.