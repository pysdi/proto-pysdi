#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 20 13:50:36 2017
@author: rrealr
"""

run_maps = True
run_ts = True


"""
# TODO: include options:
    if run_maps:
        maps_magnitud = True/False
        maps_intensity = True/False
    if run_tseries:
        tseries_area = True/False
        tseries_intensity = True/False
        tseries_magnitude = True/False

# TODO: use an .ini file instead of a .py.
"""
