#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import config_run
import subprocess

if config_run.run_maps:
    subprocess.call(
            ["python", "maps.py"], cwd="modules/maps")
if config_run.run_ts:
    subprocess.call(
            ["python", "area_dint.py"], cwd="modules/time_series")
