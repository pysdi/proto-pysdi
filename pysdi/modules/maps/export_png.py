﻿#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
export_png
Created on August/2016
Last update on June/2017

This code contains the instructions to export the results of pySDI to PNG
(Portable Network Graphics) files.

This script and the other scripts in this folder are part of the final project
of the thesis called "Monitoreo de sequías en México mediante índices
multivariados" (Drought monitoring in Mexico by mean of multivariate indices)
to obtain the degree of Master in Civil Engineering (Hydraulics) in the Program
of Master in Engineering of the National Autonomous University of Mexico
(UNAM).
"""

from grass.script import array as garray
from scipy.interpolate import griddata
import auxfunx as af
import initpar as par
import grass.script as grass
import numpy as np
import os
from platform import system


def export_png(
        data, rawres_fl, files_list, tscale, title, subtitle, color_rules_file,
        data_source):
    def set_environment():
        res = par.initial_spatial_resolution * par.spatial_resolution_factor
        rows = int((par.edge_north - par.edge_south) / res)
        cols = int((par.edge_east - par.edge_west) / res)
        grass.run_command(
                'g.region', n=par.edge_north, s=par.edge_south,
                e=par.edge_east, w=par.edge_west, rows=rows, cols=cols,
                res=res, overwrite=True, quiet=True)

        # Import auxiliary maps to GRASS-GIS
        grass.run_command(
                'v.in.ogr', input=par.states, out='states', overwrite=True,
                quiet=True)
        grass.run_command(
                'v.in.ogr', input=par.countries, out='countries',
                overwrite=True, quiet=True)

        # Set environment variables for Cairo driver
        os.environ['GRASS_RENDER_IMMEDIATE'] = 'cairo'
        os.environ['GRASS_RENDER_WIDTH'] = '800'
        os.environ['GRASS_RENDER_HEIGHT'] = '490'
        os.environ['GRASS_RENDER_BACKGROUNDCOLOR'] = 'F5F5F5'
        os.environ['GRASS_RENDER_FILE_READ'] = 'TRUE'

    def create_mask(name_out, bufdist, initial_spatial_resolution):
        main_dir = os.path.dirname(os.path.abspath('__file__'))
        shapeout = main_dir + '/' + name_out + '.shp'
        af.createBuffer(par.study_area, shapeout, bufdist)
        mask = af.vector2array(
                shapeout, initial_spatial_resolution, np.nan, par.edge_west,
                par.edge_east, par.edge_south, par.edge_north)
        mask[np.where(mask == 0)] == np.nan
        return(mask)

    def resample_data(data, coor, method, res_f=1):
        "Resample the spatial data"
        # Define the output grid
        south = par.edge_south + (par.initial_spatial_resolution * res_f * 0.5)
        north = par.edge_north
        west = par.edge_west + (par.initial_spatial_resolution * res_f * 0.5)
        east = par.edge_east
        res = par.initial_spatial_resolution * res_f
        gridlon, gridlat = np.mgrid[south:north:res, west:east:res]

        # Define the input coordinates and values.
        cells = np.shape(data)[0] * np.shape(data)[1]
        points = np.zeros((cells, 2))
        values = np.zeros((cells))
        row = 0
        for y, y_coor in enumerate(coor['lat']):
            for x, x_coor in enumerate(coor['lon']):
                points[row, 0] = y_coor
                points[row, 1] = x_coor
                values[row] = data[y, x]
                row += 1

        # Perform the resampling.
        data_res = griddata(
                points, values, (gridlon, gridlat), method=method,
                fill_value=np.nan)
        return(data_res)

    def numpy2grass(rawres_fl, files_list):
        # Export the array as a raster map to GRASS-GIS
        print("- Exporting results to GRASS GIS as raster maps")
        lon = (np.arange(par.edge_west, par.edge_east,
                         par.initial_spatial_resolution) +
               (par.initial_spatial_resolution / 2))
        lat = (np.arange(par.edge_south, par.edge_north,
                         par.initial_spatial_resolution) +
               (par.initial_spatial_resolution / 2))
        coor = {'lon': lon, 'lat': lat}
        if rawres_fl == 'raw':
            method = 'nearest'
        elif rawres_fl == 'res':
            method = 'linear'
            mask = create_mask(
                    'mask', 0.0, (par.initial_spatial_resolution *
                                  par.spatial_resolution_factor))
            mask = np.flipud(mask)
            mask = mask.astype('float16')
            mask[mask == 0] = np.nan
        for layer, values in enumerate(data):
            data_res = resample_data(
                    values, coor, method, res_f=par.spatial_resolution_factor)
            if rawres_fl == 'res':
                data_res = data_res * mask
            output_map = garray.array()
            output_map[:, :] = np.flipud(data_res[:, :])
            map_name = files_list[layer].split('/')[-1]
            output_map.write(mapname=map_name, overwrite=True)
            af.progress_bar(layer + 1, np.shape(data)[0])

    def compose_png(layer, rawres_fl, files_list, tscale, title, subtitle):
        map_name = files_list[layer].split('/')[-1]
        png_filename = files_list[layer] + '.png'
        month = png_filename[-10:-8]
        year = png_filename[-14:-10]
        if tscale == 1:
            txtm = 'mes'
        else:
            txtm = 'meses'
        date_stamp = (month + '/' + year)
        if title == 'Magnitud de sequía':
            tscale_stamp = ''
        else:
            tscale_stamp = ('Escala temporal: ' + format(tscale) + ' ' + txtm)
        os.environ['GRASS_RENDER_FILE'] = png_filename
        grass.run_command(
                'r.colors', map=map_name, rules=color_rules_file, quiet=True)
        grass.run_command('d.rast', map=map_name, quiet=True)
        grass.run_command(
                'd.vect', map='states', color='100:100:100', fill_color='none',
                width=0.25, quiet=True)
        grass.run_command(
                'd.vect', map='countries', color='100:100:100',
                fill_color='224:224:224', width=0.25, quiet=True)
        if system() == 'Linux':
            grass.run_command(
                    'd.text', flags='ps', text=title, color='0:0:0',
                    at=[20, 425], size=10,
                    path=('/usr/share/fonts/truetype/dejavu'
                          '/DejaVuSans-Bold.ttf'), charset='UTF-8')
            grass.run_command(
                    'd.text', flags='ps', text=subtitle, color='0:0:0',
                    at=[20, 440], size=9,
                    path=('/usr/share/fonts/truetype/dejavu'
                          '/DejaVuSans-Bold.ttf'), charset='UTF-8')
            grass.run_command(
                    'd.text', flags='ps', text=tscale_stamp, color='0:0:0',
                    at=[20, 455], size=9,
                    path='/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf',
                    charset='UTF-8')
            grass.run_command(
                    'd.text', flags='ps', text=date_stamp, color='0:0:0',
                    at=[675, 35], size=15,
                    path=('/usr/share/fonts/truetype/dejavu'
                          '/DejaVuSans-Bold.ttf'), charset='UTF-8')
            grass.run_command(
                    'd.text', flags='ps', text=('Fuente de los datos: ' +
                                                data_source),
                    color='0:0:0', at=[20, 470], size=8,
                    path='/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf',
                    charset='UTF-8')
        elif system() == 'Windows':
            grass.run_command(
                    'd.text', flags='ps', text=title, color='0:0:0',
                    at=[20, 425], size=10, path='C:/Windows/Fonts/arialbd.ttf',
                    charset='UTF-8')
            grass.run_command(
                    'd.text', flags='ps', text=subtitle, color='0:0:0',
                    at=[20, 440], size=9, path='C:/Windows/Fonts/arialbd.ttf',
                    charset='UTF-8')
            grass.run_command(
                    'd.text', flags='ps', text=tscale_stamp, color='0:0:0',
                    at=[20, 455], size=9, path='C:/Windows/Fonts/arial.ttf',
                    charset='UTF-8')
            grass.run_command(
                    'd.text', flags='ps', text=date_stamp, color='0:0:0',
                    at=[700, 35], size=15, path='C:/Windows/Fonts/arialbd.ttf',
                    charset='UTF-8')
            grass.run_command(
                    'd.text', flags='ps',
                    text=('Fuente de los datos: ' + data_source),
                    color='0:0:0', at=[20, 470], size=8,
                    path='C:/Windows/Fonts/arial.ttf', charset='UTF-8')

    set_environment()
    numpy2grass(rawres_fl, files_list)
    print("- Exporting PNG files.")
    for layer, values in enumerate(data):
        compose_png(layer, rawres_fl, files_list, tscale, title, subtitle)
        af.progress_bar(layer + 1, np.shape(data)[0])

    # Remove all maps exported to GRASS-GIS.
    grass.run_command(
            'g.remove', flags='f', type='all', pattern='*', quiet=True)
