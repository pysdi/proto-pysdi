# -*- coding: utf-8 -*-
"""
/pysdi/export_nc.py

Created on
----------
    August/2016

Programmer(s)
-------------
    Real-Rangel, R. A. (Institute of Engineering of UNAM, Mexico)

Purpose
-------
    This code contains the instruction for export results of PYSDI to
    NetCDF (Network Common Data Form) files.

Comments
--------
    This script and the other scripts in this folder are part of the
    final project of the thesis called "Monitoreo de sequías en México
    mediante índices multivariados" (Drought monitoring in Mexico by
    mean of multivariate indices) to obtain the degree of Master in
    Civil Engineering (Hydraulics) in the Program of Master in
    Engineering of the National Autonomous University of Mexico.
"""

from netCDF4 import Dataset
from scipy.interpolate import griddata
import auxfunx as af
import initpar as par
import numpy as np
import os
import time


def export_nc(sdi, rawres_fl, tscale, datestamp, descr, var, prefix, sufix):
    def create_mask(name_out, bufdist, res_2d):
        main_dir = os.path.dirname(os.path.abspath('__file__'))
        shapeout = main_dir + '/' + name_out + '.shp'
        af.createBuffer(par.study_area, shapeout, bufdist)
        mask = af.vector2array(
                shapeout, res_2d, np.nan, par.edge_west, par.edge_east,
                par.edge_south, par.edge_north)
        mask[np.where(mask == 0)] == np.nan

        return(mask)

    def resample_data(data, coor, method, res_f=1):
        "Resample the spatial data"

        # Define the output grid
        south = par.edge_south + (par.initial_spatial_resolution * res_f * 0.5)
        north = par.edge_north
        west = par.edge_west + (par.initial_spatial_resolution * res_f * 0.5)
        east = par.edge_east
        res = par.initial_spatial_resolution * res_f
        gridlon, gridlat = np.mgrid[south:north:res, west:east:res]

        # Define the input coordinates and values.
        cells = np.shape(data)[0] * np.shape(data)[1]
        points = np.zeros((cells, 2))
        values = np.zeros((cells))
        row = 0

        for y, y_coor in enumerate(coor['lat']):
            for x, x_coor in enumerate(coor['lon']):
                points[row, 0] = y_coor
                points[row, 1] = x_coor
                values[row] = data[y, x]
                row += 1

        # Perform the resampling.
        data_res = griddata(
                points, values, (gridlon, gridlat), method=method,
                fill_value=np.nan)

        return(data_res)

    lats = (
            np.arange(par.edge_south, par.edge_north,
                      (par.initial_spatial_resolution *
                       par.spatial_resolution_factor)) +
            ((par.initial_spatial_resolution *
              par.spatial_resolution_factor) / 2))
    lons = (np.arange(par.edge_west, par.edge_east,
                      (par.initial_spatial_resolution *
                       par.spatial_resolution_factor)) +
            ((par.initial_spatial_resolution *
              par.spatial_resolution_factor) / 2))

    # Create the nc file.
    outputs_dir = os.path.abspath(
            os.path.join(
                    os.path.dirname('__file__'), '..', '..', '..', 'outputs'))
    if not os.path.exists(outputs_dir):
        os.makedirs(outputs_dir)
    output_file = (
            outputs_dir + '/' + prefix + format(tscale, '02d') + sufix)
    ncfile_name = output_file + '_' + rawres_fl + '.nc'
    rootgrp = Dataset(ncfile_name, 'w')

    # Create the dimensions of the nc file.
    rootgrp.createDimension('time', None)
    rootgrp.createDimension('lat', len(lats))
    rootgrp.createDimension('lon', len(lons))

    # Create the variables of the nc file.
    times = rootgrp.createVariable(
            varname='time', datatype='i4', dimensions=('time',), zlib=True,
            fill_value=par.output_null_value)
    latitudes = rootgrp.createVariable(
            varname='latitude', datatype='f4', dimensions=('lat',), zlib=True,
            fill_value=par.output_null_value)
    longitudes = rootgrp.createVariable(
            varname='longitude', datatype='f4', dimensions=('lon',), zlib=True,
            fill_value=par.output_null_value)
    data_out = rootgrp.createVariable(
            varname='sdi', datatype='f4', dimensions=('time', 'lat', 'lon',),
            zlib=True, fill_value=par.output_null_value)

    # Create the attributes of the nc file.
    rootgrp.description = descr
    rootgrp.var = var
    rootgrp.history = 'Created on ' + time.ctime(time.time())
    rootgrp.source = 'Institute of Engineering UNAM'
    latitudes.units = 'Degrees north'
    longitudes.units = 'Degrees east'
    data_out.units = 'Times the standard deviation'
    times.units = 'yyyymm'

    # Write data to the netCDF variables.
    latitudes[:] = lats
    longitudes[:] = lons
    tms = datestamp
    tms = map(int, tms)
    lon = (np.arange(par.edge_west, par.edge_east,
                     par.initial_spatial_resolution) +
           (par.initial_spatial_resolution / 2))
    lat = (np.arange(par.edge_south, par.edge_north,
                     par.initial_spatial_resolution) +
           (par.initial_spatial_resolution / 2))
    coor = {'lon': lon, 'lat': lat}

    # Populate the variables time and sdi.
    # It is done in a loop due the large amount of data to export.
    print("- Generating the NetCDF file.")

    if rawres_fl == 'raw':
        method = 'nearest'

    elif rawres_fl == 'res':
        method = 'linear'
        mask = create_mask(
                'mask.shp', 0.0, (par.initial_spatial_resolution *
                                  par.spatial_resolution_factor))
        mask = np.flipud(mask)
        mask = mask.astype('float16')
        mask[mask == 0] = np.nan

    for layer, data in enumerate(sdi):
        times[layer] = tms[layer]
        data_res = resample_data(
                data, coor, method=method, res_f=par.spatial_resolution_factor)

        if rawres_fl == 'res':
            # Fill empty cells due to the source resolution.
            # This part is based on http://stackoverflow.com/questions/3104781.
            data_res = data_res * mask

        data_res[np.isnan(data_res)] = par.output_null_value
        data_out[layer, :, :] = data_res
        af.progress_bar(layer + 1, np.shape(sdi)[0])

    rootgrp.close()
