# -*- coding: utf-8 -*-
"""
/pysdi/auxfuncs.py

Created on
----------
    June/2016

Programmer(s)
-------------
    Real-Rangel, R. A. (Institute of Engineering of UNAM, Mexico)

Purpose
-------
    Contains various auxiliary functions used in sdi.py.

Comments
--------
    This script and the other scripts in this folder are part of the
    final project of the thesis called "Monitoreo de sequías en México
    mediante índices multivariados" (Drought monitoring in Mexico by
    mean of multivariate indices) to obtain the degree of Master in
    Civil Engineering (Hydraulics) in the Program of Master in
    Engineering of the National Autonomous University of Mexico.
"""


from datetime import datetime
from netCDF4 import Dataset   # http://unidata.github.io/netcdf4-python/
from requests.auth import HTTPBasicAuth
import gdal   # https://www.youtube.com/watch?v=ToTzCQL-ITk (in spanish)
import linecache
import ogr
import os
import requests
import sys


def array2asc(array, ncols, nrows, xllcorner, yllcorner, cellsize, file_name='pysdi_output', nodata_value=-9999):
    """
    Purpose
    -------
    Exports an .asc raster file containing the data of 'array'.

    Parameters
    ----------
    array (required): numpy.ndarray
        A two-dimensions array which contains the values to be exported.
    asc (optional): str
        Name of the output .asc raster file. Default is 'pysdi_output'.
    ncols (required): int
        Number of cell columns. Must be greater than 0.
    nrows (required): int
        Number of cell rows. Must be greater than 0.
    xllcorner (required): int or float
        X coordinate of the origin (by lower left corner of the cell).
        It must match with Y coordinate type.
    yllcorner (required): int or float
        Y coordinate of the origin (by lower left corner of the cell).
        It must match with X coordinate type.
    cellsize (required): int or float
        Cell size. Must be greater than 0.
    nodata_value (optional): int or float
        The input values to be NoData in the output raster. Default is
        -9999.

    Return
    ------
    none
    """
    with open(file_name+'.asc', 'w') as f:
        f.write('ncols '+str(ncols)+'\nnrows '+str(nrows)+'\nxllcorner '+str(xllcorner)+'\nyllcorner '+str(yllcorner)+'\ncellsize '+str(cellsize)+'\nnodata_value '+str(nodata_value)+'\n'+array)
    prj = file_name + '.prj'
    with open(prj, 'w') as f:
        f.write('GEOGCS["GCS_WGS_1984",DATUM["D_WGS_1984",SPHEROID["WGS_1984",6378137,298.257223563]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]]')


def asc_refs(file_name, line):
    ref = linecache.getline(file_name, line).strip().split(' ')[-1]
    return(ref)


def createBuffer(inputfn, outputBufferfn, bufferDist):
    """
    Buffers features of a layer and saves them to a new Layer.
    Source: https://pcjericks.github.io/py-gdalogr-cookbook
    """
    gdal.PushErrorHandler('CPLQuietErrorHandler')
    inputds = ogr.Open(inputfn)
    inputlyr = inputds.GetLayer()
    shpdriver = ogr.GetDriverByName('ESRI Shapefile')
    if os.path.exists(outputBufferfn):
        shpdriver.DeleteDataSource(outputBufferfn)
    outputBufferds = shpdriver.CreateDataSource(outputBufferfn)
    bufferlyr = outputBufferds.CreateLayer(outputBufferfn, geom_type=ogr.wkbPolygon)
    featureDefn = bufferlyr.GetLayerDefn()
    for feature in inputlyr:
        ingeom = feature.GetGeometryRef()
        geomBuffer = ingeom.Buffer(bufferDist)
        outFeature = ogr.Feature(featureDefn)
        outFeature.SetGeometry(geomBuffer)
        bufferlyr.CreateFeature(outFeature)


def extract_nc4_data(file_name, dataset):
    """
    Extracts the data form a NetCDF file object.

    file_name: String or file-like. The .nc file name, including the full path.
    
    dataset: String. Inidicates wich dimensions of the data would be used, and
    any attributes such as data units, along with containing the data values
    for the variable.

    Returns array 'data'. An array containing the data of the specified
    variable.
    """
    with Dataset(file_name, 'r') as f:
        dataset = f.variables[dataset][:]
        data = dataset[:].copy()
        del dataset
    return(data)


def initial_data(var):
    inputs_dir = os.path.abspath(os.path.join(os.path.dirname('__file__'), '..', 'inputs'))
    fname = inputs_dir + '\\' + var + '.csv'
    fname = fname.replace('\\', '/')
    with open(fname, 'r') as f:
        input_data = f.read()
    input_data = multistring_to_list(input_data)
    data_source = input_data[0]
    del(input_data[:1])
    input_data.sort()
    return(data_source, input_data)


def multistring_to_list(string):
    """
    Takes a string (input) and generates a list of strings (output)
    based on it.

    Parameters
    ----------
    string: String
        A list of strings contained in one multiline string.

    Return
    ------
    list_: List
        A list of strings.
    """
    string = string.strip()
    string = string.replace('"', '')
    list_ = string.splitlines()
    return(list_)


def open_close_message(openclose_fl):
    """
    Prints the opening/closing message with a time stamp.

    Parameters
    ----------
    open_close_flag: String
        A flag that indicates the function if the message to be
        displayed is an open or a close message. Valid values are:
        "open" or "close".
    """
    file_name = sys.argv[0]
    time = datetime.now().time()
    if openclose_fl == "open":
        oc_text = ' started at'
    elif openclose_fl == "close":
        oc_text = ' finished at'
    print('=' * 79)
    print(' ' + file_name + oc_text + ' {:%H:%M:%S}'.format(time))
    print('=' * 79)


def progress_bar(current, total):
    """
    Purpose
    -------
    Display a progress bar for a process performed by a loop.

    Parameters
    ----------
    current (required): int
        The value of the current step within the loop.
    total (required): int
        The total steps within the loop.

    Return
    ------
    none
    """
    progress = float(current)/total
    sys.stdout.write("\r  Progress [%-40s] %d%% (%i/%i)" % ('='*int((float(current)/total)*40), progress * 100, current, total))
    if progress < 1:
        sys.stdout.flush()
    else:
        sys.stdout.write('\n')


def vector2array(vec_in, res_2d, nd_value, x_min, x_max, y_min, y_max):
    source_ds = ogr.Open(vec_in)   # Open the data source and read in the extent.
    source_layer = source_ds.GetLayer()
    x_cells = int((x_max - x_min) / res_2d)   # Create the destination data source.
    y_cells = int((y_max - y_min) / res_2d)
    target_ds = gdal.GetDriverByName('MEM').Create('', x_cells, y_cells, gdal.GDT_Byte)
    target_ds.SetGeoTransform((x_min, res_2d, 0, y_max, 0, -res_2d))
    band = target_ds.GetRasterBand(1)
    band.SetNoDataValue(nd_value)
    gdal.RasterizeLayer(target_ds, [1], source_layer, burn_values=[1])   # Rasterize.
    array = band.ReadAsArray()   # Read as array.
    return(array)


def web_downloader(url, username, password, output):
    """
    This procedure downloads files from a given url.
    Is based on the answers published in StackOverflow in
    https://goo.gl/AFuq7p
    https://goo.gl/TFSvZL
    """
    r = requests.get(url, auth=(username, password))

    if r.status_code == 401:   # 401: Unauthorized
        r = requests.get(url, auth=HTTPBasicAuth(username, password))

    if r.status_code == 200:
       with open(output, 'wb') as out:
          for bits in r.iter_content():
              out.write(bits)
    else:
        print("Oh oh! Something went wrong.")
