# -*- coding: utf-8 -*-
"""
pysdi/legends.py

Created on
----------
    September/2016

Programmer(s)
-------------
    Real-Rangel, R.A. (Institute of Engineering of UNAM, Mexico)

Purpose
-------
    This code contains the legend for the results file of the SDI
    analysis.

Comments
--------
    This script and the other scripts in this folder are part of the
    final project of the thesis called "Monitoreo de sequías en México
    mediante índices multivariados" (Drought monitoring in Mexico by
    mean of multivariate indices) to obtain the degree of Master in
    Civil Engineering (Hydraulics) in the Program of Master in
    Engineering of the National Autonomous University of Mexico.
"""


SPI = {'title': 'Índice Estandarizado de Precipitación (SPI)',
       'subtitle': '',
       'description': 'Standardized Precipitation Index (SPI)',
       'variables': 'Precipitation'}

SRI = {'title': 'Índice Estandarizado de Escurrimiento (SRI)',
       'subtitle': '',
       'description': 'Standardized Runoff Index (SRI)',
       'variables': 'Total runoff'}

SSI = {'title': 'Índice Estandarizado de Humedad del Suelo (SSI)',
       'subtitle': '',
       'description': 'Standardized Soil Moisture Index (SSI)',
       'variables': 'Soil moisture in the rootzone'}

PreRun = {'title': 'Índice Multivariado Estandarizado de Sequía (MSDI)',
          'subtitle': 'Precipitación y escurrimiento',
          'description': 'Multivariate Standardized Drought Index (MSDI)',
          'variables': 'Precipitation and runoff'}

PreSMo = {'title': 'Índice Multivariado Estandarizado de Sequía (MSDI)',
          'subtitle': 'Precipitación y humedad del suelo',
          'description': 'Multivariate Standardized Drought Index (MSDI)',
          'variables': 'Precipitation and soil moisture in the root zone'}

PreSMoRun = {
        'title': 'Índice Multivariado Estandarizado de Sequía (MSDI)',
        'subtitle': 'Precipitación, humedad del suelo y escurrimiento',
        'description': 'Multivariate Standardized Drought Index (MSDI)',
        'variables': ('Precipitation, soil moisture in the root zone and'
                      ' runoff')}
