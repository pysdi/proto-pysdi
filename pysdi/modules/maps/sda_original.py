﻿# -*- coding: utf-8 -*-
"""
Created on
----------
    May 2016

Programmer(s)
-------------
    Real-Rangel, R.A. (Institute of Engineering of UNAM, Mexico)

Purpose
-------
    Perform the standardized drought analysis (SDA) proposed by
    Farahmand & AghaKouchak (2015). This script has been writed as a
    part of a group of scripts needed to perform the entire calculation.
    Though, this file contains the main SDA methodology.

References
----------
    Farahmand, A., AghaKouchak, A., 2015. A generalized framework for
    deriving nonparametric standardized drought indicators. Advances in
    Water Resources 76, 140–145. doi:10.1016/j.advwatres.2014.11.012.

    Hao, Z., AghaKouchak, A., 2014. A Nonparametric Multivariate Multi-
    Index Drought Monitoring Framework. Journal of Hydrometeorology 15,
    89–101. doi: 10.1175/JHM-D-12-0160.1

    Mckee, T.B., Doesken, N.J., Kleist, J., 1993. The relationship of
    drought frequency and duration to time scales. AMS 8th Conference
    on Applied Climatology 179–184. doi:citeulike-article-id:10490403.
"""

import numpy as np
from scipy import stats


def sda(td, sc=1):
    """
    Performs the standardized drought analysis (SDA)
    """
    n, numvar = np.shape(td)
    si = np.zeros(n)
    si.fill(np.NaN)
    Y = np.zeros((n))
    A1 = np.ndarray([n - sc + 1, sc])
    A1.fill(np.NaN)
    Y = np.ndarray([n - sc + 1, numvar])
    Y.fill(np.NaN)

    for variable in range(numvar):
        for i in range(sc):
            A1[:, i] = td[i:(n - (sc - (i + 1))), variable]

        Y[:, variable] = np.sum(A1, 1)   # Scaled set

    nn = len(Y)
    si1 = np.zeros(nn)

    for month in range(1, 13):
        d = Y[month-1::12]   # d is the actual set to analize
        nnn = sum(np.isfinite(d))[0]
        bp = np.zeros(np.shape(d)[0])

        for j in range(np.shape(d)[0]):
            check = d <= d[j, :]

            for k in range(np.shape(d)[0]):
                bp[j] += np.all(check[k])

        y = (bp - 0.44) / (nnn + 0.12)

        si1[month - 1::12] = y

    si1 = stats.norm.ppf(si1)
    si[sc-1:] = si1

    return(si)
