# -*- coding: utf-8 -*-
"""
Created on Tue Aug 30 14:43:45 2016

@author: RRealR
"""

import grass.script as grass
import sys


def progress_bar(current, total):
    """
    Purpose
    -------
    Display a progress bar for a process performed by a loop.

    Parameters
    ----------
    current (required): int
        The value of the current step within the loop.
    total (required): int
        The total steps within the loop.

    Return
    ------
    none
    """
    progress = float(current)/total
    sys.stdout.write(
            "\r  Progress [%-40s] %d%% (%i/%i)" %
            ('=' * int((float(current) / total)*40), progress * 100, current,
             total))

    if progress < 1:
        sys.stdout.flush()

    else:
        sys.stdout.write('\n')


def drop_all_tables():
    tables = grass.read_command('db.tables')
    tables = tables.splitlines()
    for t, table in enumerate(tables):
        grass.run_command('db.droptable', flags='f', table=table, quiet=True)
        progress_bar(t+1, len(tables))


drop_all_tables()
