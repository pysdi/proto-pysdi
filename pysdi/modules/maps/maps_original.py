﻿#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
main
Created on May/2016
Last update on June/2017

This code computes the standardized drought index (SDI) proposed by Hao &
AghaKouchak (2013) and Farahmand & AghaKouchak (2015). This approach is an
extension of the commonly used SPI proposed by McKee et al. (1993).

This script and the other scripts in this folder are part of the final project
of the thesis called "Monitoreo de sequías en México mediante índices
multivariados" (Drought monitoring in Mexico by mean of multivariate indices)
to obtain the degree of Master in Civil Engineering (Hydraulics) in the Program
of Master in Engineering of the National Autonomous University of Mexico
(UNAM).

--- References ---
* Farahmand, A., AghaKouchak, A., 2015. A generalized framework for deriving
nonparametric standardized drought indicators. Advances in Water Resources 76,
140–145.
* Hao, Z., AghaKouchak, A., 2014. A Nonparametric Multivariate Multi-Index
Drought Monitoring Framework. Journal of Hydrometeorology 15, 89–101.
* Mckee, T.B., Doesken, N.J., Kleist, J., 1993. The relationship of drought
frequency and duration to time scales. AMS 8th Confernumence on Applied
Climatology 179–184.
"""

#from bs4 import BeautifulSoup
from dm import dm
from scipy.interpolate import griddata
from scipy.spatial import distance
from sda import sda
from dateutil.relativedelta import relativedelta
from export_nc import export_nc
from export_png import export_png
from export_shp import export_shp
from export_shp_dm import export_shp_dm
from export_kml import export_kml
from export_kml_dm import export_kml_dm
import auxfunx as af
import gesdisc_credentials as cred
import fnmatch
import initpar as par
import legends as leg
import numpy as np
import os
import urllib2

if par.data_source == 'MERRA2':
    import merra_attributes as dsetpar

elif par.data_source == 'GLDAS2':
    import gldas_attributes as dsetpar

__author__ = 'Real-Rangel, R. A. (Institute of Engineering of UNAM, Mexico)'
__version__ = '0.2.0'

# TODO: Check if system have the modules installed and install all missing
# modules.


def grid(input_file):
    """
    Define a list of longitude and latitude coordinates of a dataset within the
    study area.
    """
    # Extract coordinate from a .nc or .nc4 file.
    grid_lon = af.extract_nc4_data(input_file, dsetpar.dimensions['x'])
    grid_lat = af.extract_nc4_data(input_file, dsetpar.dimensions['y'])

    # Clip the list of coordinates to the study area.
    indx_lon = np.where(
            (grid_lon > par.edge_west) * (grid_lon < par.edge_east))
    indx_lat = np.where(
            (grid_lat > par.edge_south) * (grid_lat < par.edge_north))
    indx = {'lon': indx_lon, 'lat': indx_lat}
    coor_lon = grid_lon[
            (grid_lon > par.edge_west) * (grid_lon < par.edge_east)]
    coor_lat = grid_lat[
            (grid_lat > par.edge_south) * (grid_lat < par.edge_north)]
    coor = {'lon': coor_lon, 'lat': coor_lat}
    indx['lon'] = indx['lon'][0]
    indx['lat'] = indx['lat'][0]
    return(coor, indx)


def resample_data(data, coor, method, res_f=1):
    """
    Resample the spatial data. This will be removed in future versions.
    """
    # Define the output grid
    south = par.edge_south + (par.initial_spatial_resolution * res_f * 0.5)
    north = par.edge_north
    west = par.edge_west + (par.initial_spatial_resolution * res_f * 0.5)
    east = par.edge_east
    res = par.initial_spatial_resolution * res_f
    gridlon, gridlat = np.mgrid[south:north:res, west:east:res]

    # Define the input coordinates and values.
    cells = np.shape(data)[0] * np.shape(data)[1]
    points = np.zeros((cells, 2))
    values = np.zeros((cells))
    row = 0
    for y, y_coor in enumerate(coor['lat']):
        for x, x_coor in enumerate(coor['lon']):
            points[row, 0] = y_coor
            points[row, 1] = x_coor
            values[row] = data[y, x]
            row += 1

    # Perform the resampling.
    data_res = griddata(points, values, (gridlon, gridlat), method, np.nan)
    return(data_res)


def extract_data(input_files_list, var):
    """
    Extracts the data from a datafile file and clips it to the study area.
    """
    print("- Extracting {} data from MERRA-2 datasets.".format(var))
    for ipf, input_file in enumerate(input_files_list):
        x_raw = af.extract_nc4_data(input_file, dsetpar.parameters[var])
        x_nd_corrected = np.copy(x_raw)
        cond = x_nd_corrected == dsetpar.null_value
        x_nd_corrected[cond] = np.nan
        coor, indx = grid(input_file)
        lat_range = slice(np.min(indx['lat']), (np.max(indx['lat']) + 1))
        lon_range = slice(np.min(indx['lon']), (np.max(indx['lon']) + 1))
        data = (x_nd_corrected[0, lat_range, lon_range])
        # TODO: Stop resampling input data.
        coor, indx = grid(input_file)
        if flag_rawres == 'raw':
            method = 'nearest'
        elif flag_rawres == 'res':
            method = 'linear'
        data = resample_data(data=data, coor=coor, method=method)
        data = np.expand_dims(data, 0)
        if ipf == 0:
            data_pld = np.copy(data)
        else:
            data_pld = np.vstack((data_pld, data))
        af.progress_bar(current=ipf+1, total=len(input_files_list))
    return(data_pld)


def ts_dates():
    dates = [par.ts_start]
    i = 1
    while (dates[-1]+relativedelta(months=+1)) < par.ts_end:
        dates.append(par.ts_start + relativedelta(months=+i))
        i += 1
    return(dates)


def input_files_checker(dates):
    """
    Look for input files in 'par.input_dset_dir' directory and download any
    missing file from the Goddard Earth Sciences Data and Information Services
    Center (GES DISC; https://daac.gsfc.nasa.gov/).
    """
    files_list = []
    check_year = ''   # XXX: (A)
    for date in dates:
        matches = []
        yyyy = str(date.year)   # XXX: (B)
        mm = str(date.month).zfill(2)
        pattern = '{}{}.nc4'.format(yyyy, mm)
        for root, dirnames, filenames in os.walk(par.input_dset_dir):
            for filename in fnmatch.filter(filenames, '*' + pattern):
                matches.append(os.path.join(root, filename))
        if len(matches) == 0:
            print("  No file match with '*{}' found in local directory.".
                  format(pattern))
            print("  Downloading from GES DISC.")
            if yyyy != check_year:   # XXX: Does this makes sense? (A and B)
                pass
#                html_page = urllib2.urlopen(dsetpar.product_url + '/' + yyyy)
#                soup = BeautifulSoup(html_page, 'lxml')
#                href_list = []
#                for link in soup.findAll('a'):
#                    href_list.append(link.get('href'))
#                href_list = sorted(list(set(href_list)))
            to_download = [fName for fName in href_list if pattern in fName]
            if len(to_download) < 1:
                print('  No file in GES DISC that match the pattern *{}.'.
                      format(pattern))
            else:
                for dset_file in to_download:
                    url = ('http://goldsmr4.gesdisc.eosdis.nasa.gov/opendap'
                           '/hyrax/MERRA2_MONTHLY/M2TMNXLND.5.12.4' + '/' +
                           yyyy + '/' + dset_file + '.nc?' + par.subset)
                    file_path = par.input_dset_dir + '/' + yyyy
                    if not os.path.exists(file_path):
                        os.makedirs(file_path)
                    af.web_downloader(
                            url=url,
                            username=cred.username,
                            password=cred.password,
                            output=file_path + '/' + dset_file)
                matches.append(file_path)
        files_list.append(matches[0])
    return(files_list)


def get_data(input_var, flag_rawres):
    """
    Find input files based on the file location (defined in par.input_files)
    and dates range (defined in par.ts_start and par.ts_end) and import the
    input data to a dictionary of 3-d arrays (time-lat-lon).
    """
    var_data = {}
    dates = ts_dates()
    input_files_list = input_files_checker(dates)
    for var in input_var:
        # TODO: Import subsets of MERRA-2 datasets.
        # TODO: Generates a NetCDF file for the clipped input data and only
        # import new datasets every time.
        if var == 'Total runoff':
            var_data[var] = (
                    extract_data(input_files_list, 'Baseflow') +
                    extract_data(input_files_list, 'Surface runoff'))
        else:
            var_data[var] = extract_data(input_files_list, var)
    return(var_data)


def create_mask(name_out, bufdist, initial_spatial_resolution):
    main_dir = os.path.dirname(os.path.abspath('__file__'))
    shapeout = main_dir + '/' + name_out + '.shp'
    af.createBuffer(par.study_area, shapeout, bufdist)
    mask = af.vector2array(
            shapeout, initial_spatial_resolution, np.nan, par.edge_west,
            par.edge_east, par.edge_south, par.edge_north)
    mask[np.where(mask == 0)] == np.nan
    return(mask)


def fill_blanks(data, mask, elements=10):
    val_pairs = np.column_stack(np.where(np.isfinite(data)))
    missval_mask = mask - np.isfinite(data)
    missval_mask[missval_mask != 1] = 0
    missval_pairs = np.column_stack(np.where(missval_mask))
    for mvp, missval_pair in enumerate(missval_pairs):
        missval_pair = missval_pair[np.newaxis, :]
        dist = distance.cdist(val_pairs, missval_pair, 'euclidean')
        dist = np.squeeze(dist)
        sortindx = np.argsort(dist)[:elements]
        dist = list(dist[i] for i in sortindx)
        loc = list(val_pairs[i] for i in sortindx)
        beta = 2.
        weight = np.power(dist, -beta)
        weight /= np.sum(np.power(dist, -beta))
        zi = 0
        for i in range(elements):
            zi += data[loc[i][0], loc[i][1]] * weight[i]
        data[missval_pair[0][0], missval_pair[0][1]] = zi
    return(data)


def compute_sdi(input_var, flag_rawres='raw'):
    # TODO: Move preprocessing out of this function.
    var_data = get_data(input_var, flag_rawres)
    if flag_rawres == 'res':
        for var in input_var:
            print("- Filling blank cells of {} data.".format(var))
            buf_mask = create_mask(
                    'buf_mask', 0.5, par.initial_spatial_resolution)
            buf_mask = np.flipud(buf_mask)
            for l, lay in enumerate(var_data[var]):
                var_data[var][l] = fill_blanks(lay, buf_mask)
                af.progress_bar(current=l + 1, total=len(var_data[var]))
    print("- Computing the {temporal_scale}-month SDI.".
          format(temporal_scale=temporal_scale))
    sdi = np.ndarray((np.shape(var_data.values()[0])), dtype='float16')
    # TODO: Use Numpy to compute SDI for all computational area at once.
    for row in range(np.shape(sdi)[1]):
        for col in range(np.shape(sdi)[2]):
            td = np.ndarray((np.shape(sdi)[0], len(input_var)))
            td.fill(np.nan)
            for var in range(len(input_var)):
                # Fill the time series for the SDA
                td[:, var] = (var_data.values()[var][:, row, col])
            for var in range(len(input_var)):
                # Check for cells without data (no-data values)
                if sum(np.isnan(td[:, var])) > 0:
                    sdi[:, row, col].fill(np.nan)
                    pass
            if np.isnan(sdi[0, row, col]):
                pass
            else:
                si = sda(td, temporal_scale)
                sdi[:, row, col] = si
            af.progress_bar(
                    current=(col+1)+((row)*np.shape(sdi)[2]),
                    total=np.shape(sdi)[1]*np.shape(sdi)[2])
    return(sdi)


def compute_dm(sdi):
    print("- Computing the drought magnitude")
    magnitude = np.ndarray(np.shape(sdi), dtype='float32') * np.nan
    for row in range(np.shape(sdi)[1]):
        for col in range(np.shape(sdi)[2]):
            td = np.copy(sdi[:, row, col])
            if sum(np.isnan(td)) > 0:
                pass
            else:
                magnitude[:, row, col] = dm(td, par.dm_sev_threshold)
            af.progress_bar(
                    current=(col+1)+((row)*np.shape(sdi)[2]),
                    total=np.shape(sdi)[1]*np.shape(sdi)[2])
    return(magnitude)


def file_namer(t, prefix, middle, sufix):
    "Set the date label for the file name"
    dates = ts_dates()
    date = str(dates[t].year)+str(dates[t].month).zfill(2)
    outputs_dir = os.path.abspath(
            os.path.join(
                    os.path.dirname('__file__'), '..', '..', '..', 'outputs'))
    file_name = (
            outputs_dir + '\\' + prefix + format(temporal_scale, '02d') +
            middle + '_' + date + sufix)
    file_name = file_name.replace('\\', '/')
    return(file_name)


def analysis_description(flag_rawres, descr, variables, temporal_scale):
    print("\n")
    print(("-" * 28) + " ANALYSIS DESCRIPTION " + ("-" * 29))
    print("- Index:       " + descr)
    print("- Variables:   " + variables)
    if temporal_scale == 1:
        mtxt = 'month'
    else:
        mtxt = 'months'
    print("- Time scale:  {temporal_scale} ".
          format(temporal_scale=(str(temporal_scale) + ' ' + mtxt)))
    if flag_rawres == 'raw':
        analysistxt = 'raw'
    elif flag_rawres == 'res':
        analysistxt = 'resampled'
    print("- Source data: "+dsetpar.product_name+" ("+analysistxt+" data)")
    print("-" * 79)
    print("\n")


def export_files(
        data, flag_rawres, temporal_scale, descr, variables, title, subtitle,
        color_rules_file, prefix):
    files_list = []
    for t, v_t in enumerate(data):
        if title == 'Magnitud de sequía':
            files_list.append(file_namer(t, prefix, '_dm', '_'+flag_rawres))
        else:
            files_list.append(file_namer(t, prefix, '', '_'+flag_rawres))
    if flag_rawres in par.flag_export_nc:
        # Export NetCDF (Network Common Data Form) files.
        data4nc = data[par.export_period_nc]
        dates = ts_dates()
        date_list = [str(date.year)+str(date.month).zfill(2) for date in dates]
        date_list = date_list[par.export_period_nc]
        if title == 'Magnitud de sequía':
            export_nc(
                    data4nc, flag_rawres, temporal_scale, date_list, descr,
                    variables, prefix, '_dm')
        else:
            export_nc(
                    data4nc, flag_rawres, temporal_scale, date_list, descr,
                    variables, prefix, '')
        print("- NetCDF file has been exported.")
    else:
        print("- No NetCDF file has been exported.")
    if flag_rawres in par.flag_export_png:
        # Export PNG (Portable Network Graphics) files.
        data4png = data[par.export_period_png]
        files4png = files_list[par.export_period_png]
        export_png(
                data4png, flag_rawres, files4png, temporal_scale, title,
                subtitle, color_rules_file, dsetpar.product_name)
    else:
        print("- No PNG file has been generated.")
    if flag_rawres in par.flag_export_shp:
        # Export SHP (Shapefile) files.
        data4shp = data[par.export_period_shp]
        files4shp = files_list[par.export_period_shp]
        if title == 'Magnitud de sequía':
            export_shp_dm(data4shp, flag_rawres, files4shp)
        else:
            export_shp(data4shp, flag_rawres, files4shp)
    else:
        print("- No SHP file has been generated.")
    if flag_rawres in par.flag_export_kml:
        # Export KML (Keyhole Markup Language) files.
        data4kml = data[par.export_period_kml]
        files4kml = files_list[par.export_period_kml]
        if title == 'Magnitud de sequía':
            export_kml_dm(data4kml, flag_rawres, files4kml)
        else:
            export_kml(data4kml, flag_rawres, files4kml)
    else:
        print("- No KML file has been generated.")


if __name__ == '__main__':
    af.open_close_message('open')
    index_variables = {
            'SPI': ['Precipitation'],
            'SRI': ['Total runoff'],
            'SSI': ['Soil moisture'],
            'PreRun': ['Precipitation', 'Total runoff'],
            'PreSMo': ['Precipitation', 'Soil moisture'],
            'PreSMoRun': ['Precipitation', 'Soil moisture', 'Total runoff']}
    for index in par.index:
        prefix = 'IIUNAM_{}_{}'.format(par.data_source, index)
        input_var = index_variables[index]
        for temporal_scale in par.temporal_scale:
            legends = eval('leg.' + index)
            for flag_rawres in ['raw', 'res']:
                export_fmt = ['nc', 'png', 'shp', 'kml']
                anflag = [eval('par.flag_export_'+i)[0] for i in export_fmt]
                if flag_rawres in anflag:
                    analysis_description(
                            flag_rawres, legends['description'],
                            legends['variables'], temporal_scale)
                    sdi = compute_sdi(input_var, flag_rawres)
                    if par.fl_sdi:
                        export_files(
                                sdi, flag_rawres, temporal_scale,
                                legends['description'], legends['variables'],
                                legends['title'], legends['subtitle'],
                                'color_rules_sdi.txt', prefix)
                    if (par.fl_dm) & (
                            index == 'PreSMoRun') & (
                                    temporal_scale == 1):
                        # Only computes magnitude for MSDI-PreSMoRun-01
                        magnitude = compute_dm(sdi)
                        description = (
                                'Drought magnitude based on the ' +
                                legends['description'])
                        export_files(
                                magnitude, flag_rawres, temporal_scale,
                                description, legends['variables'],
                                'Magnitud de sequía', legends['subtitle'],
                                'color_rules_dm.txt', prefix)

    # Remove the files created for the buffer mask.
    filelist = [
            f for f in os.listdir(".") if any(
                    [f.endswith(".dbf"), f.endswith(".shp"),
                     f.endswith(".shx"), f.endswith(".pyc"),
                     f.endswith(".sql")])]
    for f in filelist:
        os.remove(f)
    af.open_close_message('close')
