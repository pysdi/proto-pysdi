# -*- coding: utf-8 -*-
"""
/pysdi/dm.py

Created on
----------
    September/2016

Programmer(s)
-------------
    Real-Rangel, R. A. (Institute of Engineering of UNAM, Mexico)

Purpose
-------
    This code computes drought magnitud (DM) proposed by McKee et al.
    (1993) for the standardized precipitation index (SPI). Here, it is
    applied computing the partial DM for every time step in a time
    series of a standardized drought index. Although McKee et al.
    proposed this methodology for be used with values of SPI, it can be
    used with any other standardized drought index (e. g., SRI, SSI,
    etc.)

References
----------
    Mckee, T.B., Doesken, N.J., Kleist, J., 1993. The relationship of
    drought frequency and duration to time scales. AMS 8th Conference
    on Applied Climatology 179–184.

Comments
--------
    This script and the other scripts in this folder are part of the
    final project of the thesis called "Monitoreo de sequías en México
    mediante índices multivariados" (Drought monitoring in Mexico by
    mean of multivariate indices) to obtain the degree of Master in
    Civil Engineering (Hydraulics) in the Program of Master in
    Engineering of the National Autonomous University of Mexico.
"""


import numpy as np


def dm(sdi, sev_threshold):
    dm_timeseries = np.ndarray(np.shape(sdi))
    dm_timeseries[0] = sdi[0]
    change_flag = abs(np.append(np.nan, np.sign(sdi)[:-1]) - np.sign(sdi)) != 0

    for i in np.arange(1, len(sdi)):
        dm_timeseries[i] = np.nansum([sdi[i], dm_timeseries[i - 1]])

        if change_flag[i]:
            dm_timeseries[i] = sdi[i]

    dm_timeseries = dm_timeseries / sev_threshold
    return(dm_timeseries)
