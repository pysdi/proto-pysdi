﻿# -*- coding: utf-8 -*-
"""
Created on
----------
    May 2016

Programmer(s)
-------------
    Real-Rangel, R.A. (Institute of Engineering of UNAM, Mexico)

Purpose
-------
    Perform the standardized drought analysis (SDA) proposed by
    Farahmand & AghaKouchak (2015). This script has been writed as a
    part of a group of scripts needed to perform the entire calculation.
    Though, this file contains the main SDA methodology.

References
----------
    Farahmand, A., AghaKouchak, A., 2015. A generalized framework for
    deriving nonparametric standardized drought indicators. Advances in
    Water Resources 76, 140–145. doi:10.1016/j.advwatres.2014.11.012.

    Hao, Z., AghaKouchak, A., 2014. A Nonparametric Multivariate Multi-
    Index Drought Monitoring Framework. Journal of Hydrometeorology 15,
    89–101. doi: 10.1175/JHM-D-12-0160.1

    Mckee, T.B., Doesken, N.J., Kleist, J., 1993. The relationship of
    drought frequency and duration to time scales. AMS 8th Conference
    on Applied Climatology 179–184. doi:citeulike-article-id:10490403.
"""

import numpy as np
from scipy import stats


def sda(record_ts, variables, time_scale=1):
    """
    Performs the standardized drought analysis (SDA)
    """
    n, numvar = np.shape(record_ts)
    sdi = np.zeros(n)
    sdi.fill(np.NaN)
    Y = np.zeros((n))
    A1 = np.ndarray([n - time_scale + 1, time_scale])
    A1.fill(np.NaN)
    Y = np.ndarray([n - time_scale + 1, numvar])
    Y.fill(np.NaN)

    for v, variable in enumerate(variables):
        for i in range(time_scale):
            A1[:, i] = record_ts[i:(n - (time_scale - (i + 1))), v]

        if variable == 'Soil moisture':
            Y[:, v] = np.mean(A1, 1)   # Scaled set

        else:
            Y[:, v] = np.sum(A1, 1)   # Scaled set

    nn = len(Y)
    si1 = np.zeros(nn)

    for month in range(1, 13):
        month_ts = Y[month-1::12]   # month_ts is the actual set to analize
        N_zero = np.sum((month_ts == 0).all(axis=1))
        P_zero = N_zero / float(len(month_ts))
        d_nonzero = month_ts.copy()
        d_nonzero[(d_nonzero == 0).all(axis=1)] = np.nan
        N_nonzero = np.sum(np.isfinite(d_nonzero).all(axis=1))
        rank = np.zeros(np.shape(d_nonzero)[0])

        for j in range(len(rank)):
            check = month_ts <= month_ts[j, :]

            for k in range(len(rank)):
                rank[j] += np.all(check[k])

        P_nonzero = (rank - 0.44) / (N_nonzero + 0.12)
        P_zero = N_zero / float(N_zero + len(d_nonzero))
        P = P_zero + ((1 - P_zero) * P_nonzero)
        si1[month - 1::12] = P

    si1 = stats.norm.ppf(si1)
    sdi[time_scale-1:] = si1
    return(sdi)
