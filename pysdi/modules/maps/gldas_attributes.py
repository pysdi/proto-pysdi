#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
gldas_attributes
Created on May/2017
Last update on May/2017.
"""


__author__ = 'Real-Rangel, R. A. (Institute of Engineering of UNAM, Mexico)'
__version__ = '0.1.0'


product_shortname = 'GLDAS_NOAH025_M'

product_longname = ('GLDAS Noah Land Surface Model L4 monthly 0.25 x 0.25'
                    ' degree V2.0 and 2.1')

product_doi = '10.5067/SXAVCZFAQLNO'

product_version = ['021', '020']

product_format = 'nc4'

product_url = [
        ('https://hydro1.gesdisc.eosdis.nasa.gov/data/GLDAS'
         '/GLDAS_NOAH025_M.2.1/'),
        ('https://hydro1.gesdisc.eosdis.nasa.gov/data/GLDAS'
         '/GLDAS_NOAH025_M.2.0/')]

parameters = {
        'Baseflow': 'Qsb_acc', 'Precipitation': 'Rainf_f_tavg',
        'Runoff': 'Qs_acc', 'Soil moisture': 'RootMoist_inst'}

dimensions = {'x': 'lon', 'y': 'lat'}

null_value = -9999


def file_name(year, month, version):
    """
    year: String of four characters. Example: '1980', '2012'.
    month: String of two characters. Example: '01', '06', '12'.
    version: String of three characters. Example: '020', '021'.
    """
    return('GLDAS_NOAH025_M.A{}{}.{}.nc4'.format(year, month, version))
