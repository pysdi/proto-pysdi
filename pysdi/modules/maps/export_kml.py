# -*- coding: utf-8 -*-
"""
/pysdi/export_kml.py

Created on
----------
    August/2016

Programmer(s)
-------------
    Real-Rangel, R. A. (Institute of Engineering of UNAM, Mexico)

Purpose
-------
    This code contains the instruction for export results of PYSDI to
    KML (Keyhole Markup Language) files.

Comments
--------
    This script and the other scripts in this folder are part of the
    final project of the thesis called "Monitoreo de sequías en México
    mediante índices multivariados" (Drought monitoring in Mexico by
    mean of multivariate indices) to obtain the degree of Master in
    Civil Engineering (Hydraulics) in the Program of Master in
    Engineering of the National Autonomous University of Mexico.
"""


from grass.script import array as garray
from scipy.interpolate import griddata
import auxfunx as af
import initpar as par
import grass.script as grass
import numpy as np
import os


def export_kml(sdi, rawres_fl, files_list):
    def set_environment():
        res = par.initial_spatial_resolution * par.spatial_resolution_factor
        rows = int((par.edge_north - par.edge_south) / res)
        cols = int((par.edge_east - par.edge_west) / res)
        os.environ['GRASS_VERBOSE'] = '-1'
        os.environ['DEBUG'] = '0'
        grass.run_command(
                'g.region', n=par.edge_north, s=par.edge_south,
                e=par.edge_east, w=par.edge_west, rows=rows, cols=cols,
                res=res, overwrite=True, quiet=True)

    def create_mask(name_out, bufdist, res_2d):
        main_dir = os.path.dirname(os.path.abspath('__file__'))
        shapeout = main_dir + '/' + name_out + '.shp'
        af.createBuffer(par.study_area, shapeout, bufdist)
        mask = af.vector2array(shapeout, res_2d, np.nan, par.edge_west,
                               par.edge_east, par.edge_south, par.edge_north)
        mask[np.where(mask == 0)] == np.nan

        return(mask)

    def resample_data(data, coor, method, res_f=1):
        "Resample the spatial data"
        # Define the output grid
        south = par.edge_south + (par.initial_spatial_resolution * res_f * 0.5)
        north = par.edge_north
        west = par.edge_west + (par.initial_spatial_resolution * res_f * 0.5)
        east = par.edge_east
        res = par.initial_spatial_resolution * res_f
        gridlon, gridlat = np.mgrid[south:north:res, west:east:res]

        # Define the input coordinates and values.
        cells = np.shape(data)[0] * np.shape(data)[1]
        points = np.zeros((cells, 2))
        values = np.zeros((cells))
        row = 0

        for y, y_coor in enumerate(coor['lat']):
            for x, x_coor in enumerate(coor['lon']):
                points[row, 0] = y_coor
                points[row, 1] = x_coor
                values[row] = data[y, x]
                row += 1

        # Perform the resampling.
        data_res = griddata(points, values, (gridlon, gridlat), method=method,
                            fill_value=np.nan)

        return(data_res)

    def numpy2grass(rawres_fl, files_list):
        # Export the array as a raster map to GRASS-GIS
        print("- Exporting SDI results to GRASS GIS as raster maps.")
        lon = (
                np.arange(par.edge_west, par.edge_east,
                          par.initial_spatial_resolution) +
                (par.initial_spatial_resolution / 2))
        lat = (
                np.arange(par.edge_south, par.edge_north,
                          par.initial_spatial_resolution) +
                (par.initial_spatial_resolution / 2))
        coor = {'lon': lon, 'lat': lat}

        if rawres_fl == 'raw':
            method = 'nearest'

        elif rawres_fl == 'res':
            method = 'linear'
            mask = create_mask(
                    'mask', 0.0, par.initial_spatial_resolution *
                    par.spatial_resolution_factor)
            mask = np.flipud(mask)
            mask = mask.astype('float16')
            mask[mask == 0] = np.nan

        for t, sdi_t in enumerate(sdi):
            sdi_t_res = resample_data(
                    sdi_t, coor, method, res_f=par.spatial_resolution_factor)

            if rawres_fl == 'res':
                sdi_t_res = sdi_t_res * mask

            output_map = garray.array()
            output_map[:, :] = np.flipud(sdi_t_res[:, :])
            map_name = files_list[t].split('/')[-1]
            output_map.write(mapname=map_name, overwrite=True)
            af.progress_bar(t + 1, np.shape(sdi)[0])

    def kml_style(file_name):
        with open(file_name, 'r') as f:
            kml = f.read()

        kml = kml.splitlines()
        pmark_begin = []

        for ln, line in enumerate(kml):
            if "<Placemark>" in line:
                pmark_begin.append(ln)

        pmark_end = []

        for ln, line in enumerate(kml):
            if "</Placemark>" in line:
                pmark_end.append(ln)

        def style(hexcolor):
            style = ("\t<Style><PolyStyle><color>" + hexcolor +
                     "</color><outline>0</outline></PolyStyle></Style>")

            return(style)

        for b, begin in enumerate(pmark_begin):
            for line in range(pmark_begin[b], pmark_end[b]):
                if """<SimpleData name="label">""" in kml[line]:
                    if "D4" in kml[line]:
                        for line2 in range(pmark_begin[b], pmark_end[b]):
                            if "<Style>" in kml[line2]:
                                kml[line2] = style('992600bd')

                        break

                    elif "D3" in kml[line]:
                        for line2 in range(pmark_begin[b], pmark_end[b]):
                            if "<Style>" in kml[line2]:
                                kml[line2] = style('99203bf0')

                    elif "D2" in kml[line]:
                        for line2 in range(pmark_begin[b], pmark_end[b]):
                            if "<Style>" in kml[line2]:
                                kml[line2] = style('993c8dfd')

                    elif "D1" in kml[line]:
                        for line2 in range(pmark_begin[b], pmark_end[b]):
                            if "<Style>" in kml[line2]:
                                kml[line2] = style('995cccfe')

                    elif "D0" in kml[line]:
                        for line2 in range(pmark_begin[b], pmark_end[b]):
                            if "<Style>" in kml[line2]:
                                kml[line2] = style('99b2ffff')

                    elif "Normal" in kml[line]:
                        for line2 in range(pmark_begin[b], pmark_end[b]):
                            if "<Style>" in kml[line2]:
                                kml[line2] = style('0')

                    elif "W0" in kml[line]:
                        for line2 in range(pmark_begin[b], pmark_end[b]):
                            if "<Style>" in kml[line2]:
                                kml[line2] = style('99e8f9f0')

                    elif "W1" in kml[line]:
                        for line2 in range(pmark_begin[b], pmark_end[b]):
                            if "<Style>" in kml[line2]:
                                kml[line2] = style('99bce4ba')

                    elif "W2" in kml[line]:
                        for line2 in range(pmark_begin[b], pmark_end[b]):
                            if "<Style>" in kml[line2]:
                                kml[line2] = style('99c4cc7b')

                    elif "W3" in kml[line]:
                        for line2 in range(pmark_begin[b], pmark_end[b]):
                            if "<Style>" in kml[line2]:
                                kml[line2] = style('99caa243')

                    elif "W4" in kml[line]:
                        for line2 in range(pmark_begin[b], pmark_end[b]):
                            if "<Style>" in kml[line2]:
                                kml[line2] = style('99ac6808')

        with open(file_name, 'w') as kml_out:
            for ln, line in enumerate(kml):
                kml_out.write(line)
                kml_out.write("\n")

    def raster2vector(files_list):
        print("- Exporting KML files.")

        for f, file_name in enumerate(files_list):
            # Reclass rasters following the drought index categories
            map_in = file_name.split('/')[-1]
            map_reclass = map_in + '_reclass'
            grass.mapcalc(
                    '$output='
                    'if($input <= -2.00, 0,'
                    'if($input <= -1.60, 1,'
                    'if($input <= -1.30, 2,'
                    'if($input <= -0.80, 3,'
                    'if($input <= -0.50, 4,'
                    'if($input < 0.50, 5,'
                    'if($input < 0.80, 6,'
                    'if($input < 1.30, 7,'
                    'if($input < 1.60, 8,'
                    'if($input < 2.00, 9, 10))))))))))',
                    output=map_reclass, input=map_in, overwrite=True,
                    quiet=True)
            grass.run_command('r.to.vect', flags='sv', input=map_reclass,
                              output=map_in, type='area', overwrite=True,
                              quiet=True)
            # Update the label (W4, W3, W2, etc.) of the categories used
            valcond = {
                    'cat=0': 'D4', 'cat=1': 'D3', 'cat=2': 'D2', 'cat=3': 'D1',
                    'cat=4': 'D0', 'cat=5': 'Normal', 'cat=6': 'W0',
                    'cat=7': 'W1', 'cat=8': 'W2', 'cat=9': 'W3',
                    'cat=10': 'W4'}
            main_dir = os.path.dirname(os.path.abspath('__file__'))
            main_dir = main_dir.replace("\\", "/")

            with open(main_dir + "/update_db.sql", "w") as text_file:
                text_file.write(
                        'ALTER TABLE {} ADD COLUMN rgb varchar(11);\n'.
                        format(map_in))
                for key, val in valcond.iteritems():
                    text_file.write(
                            "UPDATE {} SET label='{}' WHERE {};\n".
                            format(map_in, val, key))

            # Add the color rules for the drought vector map
            valcond = {
                    'cat=0': '189:0:38', 'cat=1': '240:59:32',
                    'cat=2': '253:141:60', 'cat=3': '254:204:92',
                    'cat=4': '255:255:178', 'cat=5': '255:255:255',
                    'cat=6': '240:249:232', 'cat=7': '186:228:188',
                    'cat=8': '123:204:196', 'cat=9': '67:162:202',
                    'cat=10': '8:104:172'}

            with open(main_dir + "/update_db.sql", "a") as text_file:
                for key, val in valcond.iteritems():
                    text_file.write(
                            "UPDATE {} SET rgb='{}' WHERE {};\n".
                            format(map_in, val, key))

            grass.run_command('db.execute', input=main_dir+"/update_db.sql")

            os.environ['GRASS_VERBOSE'] = '-1'
            os.environ['DEBUG'] = '0'
            output = file_name + '.kml'
            grass.run_command(
                    'v.out.ogr', input=map_in, output=output, format='KML',
                    overwrite=True, quiet=True)
            kml_style(file_name=output)
            af.progress_bar(f + 1, len(files_list))

    set_environment()
    numpy2grass(rawres_fl, files_list)
    raster2vector(files_list)
    # Remove all maps exported to GRASS-GIS.
    os.environ['GRASS_VERBOSE'] = '-1'
    os.environ['DEBUG'] = '0'
    grass.run_command(
            'g.remove', flags='f', type='all', pattern='*', quiet=True)
