#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
initpar
Created on May/2016
Last update on June/2017.

This code computes the standardized drought index (SDI) proposed by Hao &
AghaKouchak (2013) and Farahmand & AghaKouchak (2015). This approach is an
extension of the commonly used SPI proposed by McKee et al. (1993).

This script and the other scripts in this folder are part of the final project
of the thesis called "Monitoreo de sequías en México mediante índices
multivariados" (Drought monitoring in Mexico by mean of multivariate indices)
to obtain the degree of Master in Civil Engineering (Hydraulics) in the Program
of Master in Engineering of the National Autonomous University of Mexico
(UNAM).

--- References ---
* Farahmand, A., AghaKouchak, A., 2015. A  generalized framework for deriving
nonparametric standardized drought indicators. Advances in Water Resources 76,
140–145.
* Hao, Z., AghaKouchak, A., 2014. A Nonparametric Multivariate Multi-Index
Drought Monitoring Framework. Journal of Hydrometeorology 15, 89–101.
* Mckee, T.B., Doesken, N.J., Kleist, J., 1993. The relationship of drought
frequency and duration to time scales. AMS 8th Conference on Applied
Climatology 179–184.
"""

from datetime import date

"""
SDI analysis options
---
ts_start: Date. Date of the first value of the time series.
ts_end: Date. Date of the last value of the time series.
export_fl_an: String. Flag to define which analysis to perform. Standardized
    index ['si'], drought magnitude ['dm'], or both ['dm', 'si'].
dm_sev_threshold: Float. If the variable 'export_fl_an' contains 'dm',
    this variable defines the treshold of intensity to define the magnitude of
    an event.
index: String. Index to be analized: Standardized Precipitation Index ['SPI'],
    Standardized Runoff Index ['SRI'], Standardized Soil Moisture Index
    ['SSI'], Multivariate Standardized Drought Index (MSDI) for precipitation
    and runoff ['PreRun'], for precipitation and soil moisture ['PreSMo'] or
    for precipitation, soil moisture and runoff ['PreSMoRun'].
input_var: List. List of strings of the name of the variables to be analized.
    It is defined by the index to be computed (variable 'index').
temporal_scale: List of integers. List of time scales to be analized. The list
can include as many time scales as needed, e.g., temporal_scale = [1],
temporal_scale = [1, 3,
    12], etc.
output_null_value: Integer. Defines the no-data cell value for the output
    raster maps.
spatial_resolution_factor: Float. Factor by wich will be multiplied the
    original resolution of the input raster maps to produce the output raster
    maps.
prefix: String. Initial string for the name of the output files.
"""
ts_start = date(1980, 1, 1)
# TODO: Set ts_end to date.today() to automatize the process.
ts_end = date(2018, 11, 1)
fl_contmiss = True  # TODO: Continue if some file is missing.
fl_sdi = True  # Export standardized drought index results.
fl_dm = True  # Export drought magnitude results.
dm_sev_threshold = 1.3
output_null_value = -32768
spatial_resolution_factor = 0.25
index = ['PreSMoRun', 'SPI', 'SRI', 'SSI', 'PreSMo', 'PreRun']
temporal_scale = [1, 3, 6, 9, 12]
data_source = 'MERRA2'


"""
Computational region
---
edge_north: Float or integer. Latitud coordinate of the northern edge.
edge_south: Float or integer. Latitud coordinate of the southern edge.
edge_east: Float or integer. Longitude coordinate of the eastern edge.
edge_west: Float or integer. Longitude coordinate of the western edge.
initial_spatial_resolution: Float or integer. Size of the cell of the
    computational region. dx = dy.
"""

edge_north = 34
edge_south = 14
edge_east = -86
edge_west = -119
initial_spatial_resolution = 0.25


"""
Export options
---
What to export. Use 'raw' and/or 'res' (e.g., ['raw'], ['raw', 'res'], etc.).
    If none of the analysis is needed, use [''].
Which period to export. Use slice objects. The format is
    slice(Start, Stop, Step).
    Examples:
        'a' would be the list of periods
            a = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].
        To export all periods:
            a[slice(0, None, None)] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        To export from period 0 to period 5
            a[slice(None, 5, None)] = [0, 1, 2, 3, 4]
        To export only the last period
            a[slice(-1, None, None)] = [9]
"""
flag_export_nc = ['res']
flag_export_png = ['res']
flag_export_shp = ['res']
flag_export_kml = ['res']
export_period_nc = slice(0, None, None)
export_period_png = slice(-1, None, None)
export_period_shp = slice(-1, None, None)
export_period_kml = slice(-1, None, None)


"""
Files to import
---
study_area: String. Path to the shapefile that contains the polygone of the
    study area.
water_bods: String. Path to the shapefile that contains the polygone of the
    water bodies in the study area.
states: String. Path to the shapefile that contains the polygone of the
    political boundaries in the study area.
countries: String. Path to the shapefile that contains the polygone of the
    neighbor countries of the study area.
color_rules: String. Text file (TXT) that contains the color rules for the
    raster generated.
"""
# TODO: Define lonlat subset from n_, s_, e_, edge_west variables.
subset = (
        'BASEFLOW[0:0][208:248][98:150],'
        'RUNOFF[0:0][208:248][98:150],'
        'SMLAND[0:0][208:248][98:150],'
        'GWETTOP[0:0][208:248][98:150],'
        'TSURF[0:0][208:248][98:150],'
        'PRECTOTLAND[0:0][208:248][98:150],'
        'RZMC[0:0][208:248][98:150],'
        'lat[208:248],'
        'time[0:0],'
        'lon[98:150]')

"""
http://goldsmr4.gesdisc.eosdis.nasa.gov/opendap/MERRA2_MONTHLY/
M2TMNXLND.5.12.4/2018/MERRA2_400.tavgM_2d_lnd_Nx.201803.nc4.nc?
BASEFLOW[0:0][208:248][98:150],RUNOFF[0:0][208:248][98:150],
SMLAND[0:0][208:248][98:150],GWETTOP[0:0][208:248][98:150],
TSURF[0:0][208:248][98:150],PRECTOTLAND[0:0][208:248][98:150],
RZMC[0:0][208:248][98:150],lat[208:248],time[0:0],lon[98:150]
"""

input_dset_dir = 'C:/Users/rrealr/OneDrive/datasets/merra/M2TMNXLND_MEX'
geoinfo_dir = 'C:/Users/rrealr/OneDrive/geoinfo'
study_area = (
        geoinfo_dir + '/division_politica/limite/'
        'limite_nacional_1a1000000/datos/contdv1mgw/contdv1mgw.shp')
states = (
        geoinfo_dir + '/division_politica/estatal'
        '/division_politica_estatal_1a250000_2015/datos/dest_2015gw'
        '/dest_2015gw.shp')
countries = (
        geoinfo_dir + '/division_politica/internacional/'
        'limites_internacionales_esri/paises_vecinos.shp')
