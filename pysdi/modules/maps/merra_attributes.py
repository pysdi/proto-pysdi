#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
merra_attributes
Created on May/2017
Last update on May/2017.
"""

product_name = ('MERRA-2 Land Surface Diagnostics V5.12.4')

product_shortname = 'M2TMNXLND'

product_longname = (
        'MERRA-2 tavgM_2d_lnd_Nx: 2d, Monthly mean, Time-Averaged,'
        ' Single-Level, Assimilation, Land Surface Diagnostics V5.12.4')

product_doi = '10.5067/8S35XF81C28F'

product_version = '5124'

product_format = 'nc4'

product_url = ('https://goldsmr4.gesdisc.eosdis.nasa.gov/data/MERRA2_MONTHLY'
               '/M2TMNXLND.5.12.4')

parameters = {
        'Baseflow': 'BASEFLOW', 'Precipitation': 'PRECTOTLAND',
        'Surface runoff': 'RUNOFF', 'Soil moisture': 'RZMC',
        'Soil surface moisture': 'GWETTOP', 'Surface temperature': 'TSURF'}

dimensions = {'x': 'lon', 'y': 'lat'}

null_value = 999999986991104
