# -*- coding: utf-8 -*-
"""
Created on: October/2016
Programmer: Real-Rangel, R. A. (Institute of Engineering UNAM; Mexico)
"""

import auxfunx as af
import gdal
import initpar as par
import numpy as np
import ogr
import os


def issue_warning(warning_type):
    warning_messages = {
            'Big_NetCDF': ("Warning: Full NetCDF file size cannot be handled "
                           "due to lack of memory capacity. It will be "
                           "imported by parts."),
            'Not_equal_arrays': ("Fatal error: Different ammount of cells "
                                 "found in each time.")}
    print(" " * 8 + warning_messages[warning_type])


def existent_paths(source_directory, extension_pattern):
    """
    Generates a list of paths in the source_directory that match with the given
    extension_pattern. It includes all subdirectories in the source_directory.
    """
    paths = []
    for path, subdirs, files in os.walk(source_directory):
        for name in files:
            if extension_pattern in name:
                paths.append(os.path.join(path, name))
    return(sorted(paths))


def cells_per_drought_cat(sdi_array):
    sdi_array[np.where(sdi_array == par.null_value)] = np.nan
    return({'d4': sdi_array <= -2,
            'd3': (-2 < sdi_array) & (sdi_array <= -1.6),
            'd2': (-1.6 < sdi_array) & (sdi_array <= -1.3),
            'd1': (-1.3 < sdi_array) & (sdi_array <= -0.8),
            'd0': (-0.8 < sdi_array) & (sdi_array <= -0.5),
            'normal': (-0.5 < sdi_array) & (sdi_array < 0.5),
            'w0': (0.5 <= sdi_array) & (sdi_array < 0.8),
            'w1': (0.8 <= sdi_array) & (sdi_array < 1.3),
            'w2': (1.3 <= sdi_array) & (sdi_array < 1.6),
            'w3': (1.6 <= sdi_array) & (sdi_array < 2),
            'w4': sdi_array >= 2})


def area_timeseries(time, drought_category):
    d4 = np.sum(drought_category['d4'], axis=(1, 2))
    d3 = np.sum(drought_category['d3'], axis=(1, 2))
    d2 = np.sum(drought_category['d2'], axis=(1, 2))
    d1 = np.sum(drought_category['d1'], axis=(1, 2))
    d0 = np.sum(drought_category['d0'], axis=(1, 2))
    normal = np.sum(drought_category['normal'], axis=(1, 2))
    w0 = np.sum(drought_category['w0'], axis=(1, 2))
    w1 = np.sum(drought_category['w1'], axis=(1, 2))
    w2 = np.sum(drought_category['w2'], axis=(1, 2))
    w3 = np.sum(drought_category['w3'], axis=(1, 2))
    w4 = np.sum(drought_category['w4'], axis=(1, 2))
    data = np.vstack((d4, d3, d2, d1, d0, normal, w0, w1, w2, w3, w4)).T

#    if len(np.unique(np.sum(data, 1))) > 1:
#        issue_warning('Not_equal_arrays')

    cell_number = np.unique(np.sum(data, 1))[-1]
    data = (data.astype('float') / cell_number) * 100
    return(np.insert(data, 0, time, 1))


def perceptile_timeseries(time, input_dataset):
    input_dataset[np.where(input_dataset == par.null_value)] = np.nan
    p000 = np.nanpercentile(input_dataset, 0, axis=(1, 2))
    p025 = np.nanpercentile(input_dataset, 25, axis=(1, 2))
    p050 = np.nanpercentile(input_dataset, 50, axis=(1, 2))
    p075 = np.nanpercentile(input_dataset, 75, axis=(1, 2))
    p100 = np.nanpercentile(input_dataset, 100, axis=(1, 2))
    data = np.vstack((p000, p025, p050, p075, p100)).T
    return(np.insert(data, 0, time, 1))


def vector2array(layer):
    xCells = int((par.edge_east - par.edge_west) / par.spatial_resolution)
    yCells = int((par.edge_north - par.edge_south) / par.spatial_resolution)
    # Create the destination data source
    targetDS = (gdal.GetDriverByName('MEM').
                Create('', xCells, yCells, gdal.GDT_Byte))
    targetDS.SetGeoTransform(
            (par.edge_west, par.spatial_resolution, 0, par.edge_north, 0,
             - par.spatial_resolution))
    band = targetDS.GetRasterBand(1)
    band.SetNoDataValue(par.null_value)
    # Rasterize
    err = gdal.RasterizeLayer(targetDS, [1], layer, burn_values=[1],
                              options=['ALL_TOUCHED=TRUE'])

    if err != 0:
        print("error:", err)

    # Read as array
    array = band.ReadAsArray()
    array = array.astype(float)
    array[np.where(array == 0)] = np.nan
    return(np.flipud(array))


def main():
    """
    Source:
    https://pcjericks.github.io/py-gdalogr-cookbook/vector_layers.html
    """
    gdal.PushErrorHandler('CPLQuietErrorHandler')
    # List SDI NC files in input directories.
    dass_pattern_sdi = 'PreSMoRun01_res.nc'
    dass_existent_paths_sdi = existent_paths(
            par.dass_source_directory, dass_pattern_sdi)
    # List Magnitude NC files in input directories.
    dass_pattern_mag = 'PreSMoRun01_dm_res.nc'
    dass_existent_paths_mag = existent_paths(
            par.dass_source_directory, dass_pattern_mag)
    # Join paths lists
    dass_existent_paths = dass_existent_paths_sdi[:]
    for i in dass_existent_paths_mag:
        dass_existent_paths.append(i)
    dass_existent_paths = [i for i in dass_existent_paths if i is not None]
    # List SHP files in input directories.
    maps_pattern = '.shp'
    maps_existent_paths = existent_paths(
            par.maps_source_directory, maps_pattern)
    maps_existent_names = [
            name.replace(par.maps_source_directory, '')
            for name in maps_existent_paths]
    maps_existent_names = [
            name.replace(maps_pattern, '')
            for name in maps_existent_names]

    for vector_map in maps_existent_names:
        file_path = par.maps_source_directory + vector_map + '.shp'
        vector_ds = ogr.Open(file_path)
        file_name = file_path.split('/')[-1]
        print("- Shapefile '{}' has been read.".format(file_name))
        lyr = vector_ds.GetLayer(0)
        # Get a field definition from the original vector file
        feature = lyr.GetFeature(0)
        field = feature.GetFieldDefnRef(0)
        # Reset the original layer so we can read all features
        lyr.ResetReading()
        feature_number = lyr.GetFeatureCount()
        print("- Rasterizing features and exporting reports.")

        for f, feat in enumerate(lyr):
            # We define an output in-memory OGR dataset
            drv = ogr.GetDriverByName('Memory')
            dst_ds = drv.CreateDataSource('out')
            dst_layer = dst_ds.CreateLayer('', geom_type=ogr.wkbPolygon)
            # Apply the field definition from the original to the output
            dst_layer.CreateField(field)
            feature_defn = dst_layer.GetLayerDefn()
            # For each feature, get the geometry
            geom = feat.GetGeometryRef()
            # Create an output feature
            out_geom = ogr.Feature(feature_defn)
            out_geom.SetGeometry(geom)
            # Add the feature with its geometry to the output layer
            dst_layer.CreateFeature(out_geom)
            mask = vector2array(dst_layer)

            # Get data from SDI NetCDF files in par.source_directory
            for path in dass_existent_paths:
                ts_time = af.extract_nc4(path, 'time')
                try:
                    # Apply mask to input SDI data
                    input_array = af.extract_nc4(path, par.data_set) * mask
                    input_array = np.ma.getdata(input_array)
                    input_array[
                            np.where(input_array == par.null_value)] = np.nan
                    # Compute time series
                    if '_dm_' in path:  # If is magnitude dataset
                        dmag_out = perceptile_timeseries(ts_time, input_array)
                        del(input_array)
                    else:
                        drought_category = cells_per_drought_cat(input_array)
                        area_out = area_timeseries(ts_time, drought_category)
                        dint_out = perceptile_timeseries(ts_time, input_array)
                        del(drought_category, input_array)
                except MemoryError:
                    # Getting data by parts
                    # issue_warning('Big_NetCDF')
                    parts = len(ts_time) / 100.
                    parts = int(parts) + (not parts.is_integer())
                    part_start = np.arange(parts) * 100
                    part_end = part_start + 100
                    for time_from, time_to in zip(part_start, part_end):
                        time_to = None if time_to > len(ts_time) else time_to
                        input_array_part = af.extract_nc4(
                                path, par.data_set, time_from, time_to) * mask
                        time_part = af.extract_nc4(
                                path, 'time', time_from, time_to)
                        if '_dm_' in path:  # If is magnitude dataset
                            if time_from == 0:
                                dmag_out = perceptile_timeseries(
                                        time_part, input_array_part)
                            else:
                                dmag_out = np.vstack(
                                        (dmag_out, perceptile_timeseries(
                                                time_part, input_array_part)))
                            del(input_array_part)
                        else:
                            drought_category_part = cells_per_drought_cat(
                                    input_array_part)
                            if time_from == 0:
                                area_out = area_timeseries(
                                        time_part, drought_category_part)
                                dint_out = perceptile_timeseries(
                                        time_part, input_array_part)
                            else:
                                area_out = np.vstack(
                                        (area_out, area_timeseries(
                                                time_part,
                                                drought_category_part)))
                                dint_out = np.vstack(
                                        (dint_out, perceptile_timeseries(
                                                time_part, input_array_part)))
                            del(drought_category_part, input_array_part)

                area_header = 'date,d4,d3,d2,d1,d0,normal,w0,w1,w2,w3,w4'
                dint_header = 'date,p0,p25,p50,p75,p100'
                feature_id_01 = feat.GetField('ID_01')
                feature_id_02 = feat.GetField('ID_02')
                try:
                    os.makedirs(par.output_directory + vector_map)
                except:
                    pass
                area_out_file_name = (
                        par.output_directory + vector_map + '/area_{}_{}.csv'.
                        format(feature_id_01, feature_id_02))
                dint_out_file_name = (
                        par.output_directory + vector_map + '/dint_{}_{}.csv'.
                        format(feature_id_01, feature_id_02))
                dmag_out_file_name = (
                        par.output_directory + vector_map + '/dmag_{}_{}.csv'.
                        format(feature_id_01, feature_id_02))
                np.savetxt(area_out_file_name, area_out, delimiter=',',
                           header=area_header, fmt='%f')
                np.savetxt(dint_out_file_name, dint_out, delimiter=',',
                           header=dint_header, fmt='%f')
                if '_dm_' in path:  # If is magnitude dataset
                    np.savetxt(dmag_out_file_name, dmag_out, delimiter=',',
                               header=dint_header, fmt='%f')
                af.progress_bar(f+1, feature_number, feature_id_01)

            # Clear things up
            out_geom.Destroy
            geom.Destroy
            # Reset the output layer to the 0th geometry
            dst_layer.ResetReading()



# RUN CODE
af.open_close_message('open')
main()
af.remfiles('.pyc')
af.open_close_message('close')
