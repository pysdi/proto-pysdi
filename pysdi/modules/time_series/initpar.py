#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
../area_ts/initpar.py
Created on June/2017
Last update on June/2017

Description:

References:

"""

import os

dass_source_directory = os.path.abspath(
        os.path.join(
                os.path.dirname('__file__'),
                '..', '..', '..', 'outputs'))

maps_source_directory = os.path.abspath(
        os.path.join(
                os.path.dirname('__file__'),
                '..', '..', '..', 'inputs'))

output_directory = os.path.abspath(
        os.path.join(
                os.path.dirname('__file__'),
                '..', '..', '..', 'outputs'))

data_set = 'sdi'
spatial_resolution = 0.0625
null_value = -32768
edge_west = -119
edge_east = -86
edge_south = 14
edge_north = 34
