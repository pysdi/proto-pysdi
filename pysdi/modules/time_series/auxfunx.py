# -*- coding: utf-8 -*-
"""
auxfuncs.py
Created on Jun 17 2016
@author: Real-Rangel, R.A. (Institute of Engineering of UNAM, Mexico)
Description:
    Contains various auxiliary functions used in sdi.py.
See also:
    sdi.py
"""

from datetime import datetime
from netCDF4 import Dataset   # http://unidata.github.io/netcdf4-python/
#from pyhdf.SD import SD, SDC   # http://hdfeos.org/software/pyhdf.php
import gdal   # https://www.youtube.com/watch?v=ToTzCQL-ITk (in spanish)
import linecache
import numpy as np
import ogr
import os
import sys


sys.dont_write_bytecode = True   # Prevent the creation of PYC files.


def array2asc(array, ncols, nrows, xllcorner, yllcorner, cellsize,
              file_name='pysdi_output', nodata_value=-9999):
    """
    Purpose
    -------
    Exports an .asc raster file containing the data of 'array'.

    Parameters
    ----------
    array (required): numpy.ndarray
        A two-dimensions array which contains the values to be exported.
    asc (optional): str
        Name of the output .asc raster file. Default is 'pysdi_output'.
    ncols (required): int
        Number of cell columns. Must be greater than 0.
    nrows (required): int
        Number of cell rows. Must be greater than 0.
    xllcorner (required): int or float
        X coordinate of the origin (by lower left corner of the cell).
        It must match with Y coordinate type.
    yllcorner (required): int or float
        Y coordinate of the origin (by lower left corner of the cell).
        It must match with X coordinate type.
    cellsize (required): int or float
        Cell size. Must be greater than 0.
    nodata_value (optional): int or float
        The input values to be NoData in the output raster. Default is
        -9999.

    Return
    ------
    none
    """
    with open(file_name + '.asc', 'w') as f:
        f.write('ncols ' + str(ncols) + '\nnrows ' + str(nrows) +
                '\nxllcorner ' + str(xllcorner) + '\nyllcorner ' +
                str(yllcorner) + '\ncellsize ' + str(cellsize) +
                '\nnodata_value ' + str(nodata_value) + '\n' + array)

    prj = file_name + '.prj'

    with open(prj, 'w') as f:
        f.write('GEOGCS["GCS_WGS_1984",DATUM["D_WGS_1984",'
                'SPHEROID["WGS_1984",6378137,298.257223563]],'
                'PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]]')


def asc_refs(file_name, line):
    ref = linecache.getline(file_name, line).strip().split(' ')[-1]

    return(ref)



def createBuffer(inputfn, outputBufferfn, bufferDist):
    """
    Buffers features of a layer and saves them to a new Layer.
    Source: https://pcjericks.github.io/py-gdalogr-cookbook
    """
    gdal.PushErrorHandler('CPLQuietErrorHandler')
    inputds = ogr.Open(inputfn)
    inputlyr = inputds.GetLayer()
    shpdriver = ogr.GetDriverByName('ESRI Shapefile')

    if os.path.exists(outputBufferfn):
        shpdriver.DeleteDataSource(outputBufferfn)

    outputBufferds = shpdriver.CreateDataSource(outputBufferfn)
    bufferlyr = outputBufferds.CreateLayer(outputBufferfn,
                                           geom_type=ogr.wkbPolygon)
    featureDefn = bufferlyr.GetLayerDefn()

    for feature in inputlyr:
        ingeom = feature.GetGeometryRef()
        geomBuffer = ingeom.Buffer(bufferDist)
        outFeature = ogr.Feature(featureDefn)
        outFeature.SetGeometry(geomBuffer)
        bufferlyr.CreateFeature(outFeature)


def extract_nc4(file_name, dataset, time_from=None, time_to=None):
    """
    Extracts the data form a NetCDF file object.

    Parameters
    ----------
    file_name: String or file-like
        The .nc file name, including the full path.
    dataset: String
        Inidicates wich dimensions of the data would be used, and any
        attributes such as data units, along with containing the data
        values for the variable.

    Return
    ------
    data: Array
        An array containing the data of the specified variable.
    """
    with Dataset(file_name, 'r') as f:
        raw_data = f.variables[dataset][time_from:time_to]
        data = raw_data[:].copy()
        data = np.asarray(data)
    return(data)


def initial_data(variable_number):
    """
    Purpose
    -------
    Imports the list of data set files.

    Parameters
    ----------
    variable_number: int
        Required. Number of the variable to which obtain the attributes
        and the list of files. Possible values include 1 or 2.
        Theorethically, the code can works with multiple variables,
        i.e., more than one. Nevertheless, it has been tested only with
        one and two variables.

    Return
    ------
    var: dict
        Dictionary with two keys: 'attributes' and 'files'.
        'attributes' is a dictionary of the main attributes of the
        variable dataset (e.g., alias, name, x dimension, y dimension,
        etc.). 'files' is a list of names of the files from which the
        datasets will be extracted and the date stamp associated with
        them.
    """
    MAIN_DIR = os.path.dirname(os.path.abspath('__file__'))
    DATA_FILE = MAIN_DIR + '/init_' + str(variable_number) + '.dat'

    with open(DATA_FILE, 'r') as f:
        input_data = f.read()

    input_data = multistring_to_list(input_data)
    del(input_data[:1])
    return(input_data.sort())


def multistring_to_list(string):
    """
    Takes a string (input) and generates a list of strings (output)
    based on it.

    Parameters
    ----------
    string: String
        A list of strings contained in one multiline string.

    Return
    ------
    list_: List
        A list of strings.
    """
    string = string.strip()
    string = string.replace('"', '')
    return(string.splitlines())


def open_close_message(openclose_fl):
    """
    Prints the opening/closing message with a time stamp.

    Parameters
    ----------
    open_close_flag: String
        A flag that indicates the function if the message to be
        displayed is an open or a close message. Valid values are:
        "open" or "close".
    """
    file_name = sys.argv[0].split('/')[-1]
    time = datetime.now().time()

    if openclose_fl == "open":
        oc_text = ' started at'

    elif openclose_fl == "close":
        oc_text = ' finished at'

    print('=' * 79)
    print(file_name + oc_text + ' {:%H:%M:%S}'.format(time))
    print('=' * 79)


def progress_bar(current, total, identifier="No ID"):
    """
    Purpose
    -------
    Display a progress bar for a process performed by a loop.

    Parameters
    ----------
    current (required): int
        The value of the current step within the loop.
    total (required): int
        The total steps within the loop.

    Return
    ------
    none
    """
    progress = float(current)/total
    sys.stdout.write("\r  Progress [{:40s}] {:.2f}% ({}/{}) [ID: {}]".
                     format('='*int((float(current)/total)*40), progress * 100,
                            current, total, identifier))

    if progress < 1:
        sys.stdout.flush()

    else:
        sys.stdout.write('\n')


def remfiles(extension):
    filelist = [ f for f in os.listdir(".") if any([f.endswith(extension)])]

    for f in filelist:
        os.remove(f)


def vector2array(vec_in, res_2d, nd_value, x_min, x_max, y_min, y_max):
    # Open the data source and read in the extent
    source_ds = ogr.Open(vec_in)
    source_layer = source_ds.GetLayer()
    
    # Create the destination data source
    x_cells = int((x_max - x_min) / res_2d)
    y_cells = int((y_max - y_min) / res_2d)
    target_ds = gdal.GetDriverByName('MEM').Create('', x_cells, y_cells,
                                                   gdal.GDT_Byte)
    target_ds.SetGeoTransform((x_min, res_2d, 0, y_max, 0, -res_2d))
    band = target_ds.GetRasterBand(1)
    band.SetNoDataValue(nd_value)
    
    # Rasterize
    gdal.RasterizeLayer(target_ds, [1], source_layer, burn_values=[1])
    
    # Read as array
    array = band.ReadAsArray()
    return(np.flipud(array))